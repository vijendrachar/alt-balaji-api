console.log('Hello form the JS file.');

// The Base URL to get the content
const baseURL = 'https://api.cloud.altbalaji.com';

// Declacrations and Initilizationss
let listArray;
const theExternalIDs = [];
const actualData = [];
const allCategories = [];

const cardDiv = document.getElementById('cards');

const renderCard = (category, titles) => {
  const card = `
    <h1 class="category">${category}</h1>  
    <br/>
    ${titles.map((ele) => {
      return `
        <div class="card" >
            <img src="${ele?.images[0].url}" alt="${ele?.title}" >
            <div class="text-center">
                <h3>${ele?.title} ${ele.releaseYear ? `(${ele.releaseYear})` : ''}</h3></br />
                <h5>Total Runtime ${Math.round(ele.length / 60)} Mins</h5>
                <p>${ele.longDescription}</p>
                <h5 class="cast">${
                  ele.credits.length > 0
                    ? `Cast - ${ele.credits.map((actor) => {
                        return ' ' + actor.name;
                      })}`
                    : ''
                }</h5>
            </div>
        </div> 
        `;
    })}
  `;

  cardDiv.insertAdjacentHTML('afterbegin', card);
};

// Function to store the data and its category
const getDataByCategory = async (url) => {
  return await axios.get(url);
};

// The fuction to get all the External IDs
const getInitialData = async () => {
  const testArr = [];

  const res = await axios.get('https://api.cloud.altbalaji.com/sections/31?domain=IN&limit=50');
  data = res.data.lists;
  const finised = await data.map(async (ele) => {
    if (ele.visible === 'true') {
      theExternalIDs.push(`${baseURL}${ele.external_id}`);
      const testData = await getDataByCategory(`${baseURL}${ele.external_id}`);
      testArr.push({ category: ele.titles.default, data: testData.data.content });
    }
  });
  await Promise.all(finised);

  return testArr;
};

getInitialData().then((data) => {
  data.forEach((ele) => {
    renderCard(ele?.category, ele?.data);
  });

  data.forEach((ele) => {
    ele.data.forEach((dataele) => {
      console.log(dataele);
    });
    console.log('--------------');
  });
});
